<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "url".
 *
 * @property int $id
 * @property int $user_id
 * @property string $long_url
 * @property string $short_code
 * @property string $time_create
 * @property string $time_end
 * @property int $counter
 *
 * @property Users $user
 */
class Url extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'url';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'long_url', 'short_code', 'time_create', 'time_end', 'counter'], 'required'],
            [['id', 'user_id', 'counter'], 'integer'],
            [['time_create', 'time_end'], 'safe'],
            [['long_url'], 'string', 'max' => 100],
            [['short_code'], 'string', 'max' => 50],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'long_url' => 'Long Url',
            'short_code' => 'Short Code',
            'time_create' => 'Time Create',
            'time_end' => 'Time End',
            'counter' => 'Counter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
