<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to login:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <div class="form-group field-loginform-username required">
            <label class="col-lg-1 control-label" for="loginform-username">Fullname</label>
            <div class="col-lg-3"><input type="text" class="form-control" name="data[fullname]" autofocus aria-required="true"></div>
        </div>

        <div class="form-group field-loginform-username required">
            <label class="col-lg-1 control-label" for="loginform-username">Username</label>
            <div class="col-lg-3"><input type="text" class="form-control" name="data[username]" autofocus aria-required="true"></div>
        </div>

        <div class="form-group field-loginform-password required">
            <label class="col-lg-1 control-label" for="loginform-password">Password</label>
            <div class="col-lg-3"><input type="password" class="form-control" name="data[password]" value="" aria-required="true"></div>
            <div class="col-lg-8"><p class="help-block help-block-error "></p></div>
        </div>


        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
