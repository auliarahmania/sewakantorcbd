<div class="cbd-default-index">
    <table class="table">
    <thead>
      <tr>
        <th>Long URL</th>
        <th>Short URL</th>
        <th>Count</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach($data as $key => $value){?>
      <tr>
        <td><?= $value->long_url?></td>
        <td><?= $value->short_code?></td>
        <td><?= $value->counter?></td>
      </tr>
      <?php }?>
    </tbody>
  </table>
</div>
