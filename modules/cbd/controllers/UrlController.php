<?php

namespace app\modules\cbd\controllers;

use Yii;
use app\models\NixShortUrls;
use app\models\NixShortUrlsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use app\models\NixUserInfo;
use app\models\Url;

class UrlController extends \yii\web\Controller
{
    public function actionIndex()
    {
		return $this->render('index');
    }

    public function actionDetail(){
		$data = Url::find()->all();

    	return $this->render('detail', array(
    		'data' => $data
    	));
    }

}
